package com.Richest.baseball.rich.player.app.screens

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.Richest.baseball.rich.player.app.databinding.ActivityBaseballSplashScreenBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class BaseballSplashScreen : AppCompatActivity() {
    private lateinit var baseballSplashScreenBinding: ActivityBaseballSplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        baseballSplashScreenBinding = ActivityBaseballSplashScreenBinding.inflate(layoutInflater)
        setContentView(baseballSplashScreenBinding.root)
        setBaseballFullScreen()

        GlobalScope.launch {
            delay(2400)
            val intent = Intent(this@BaseballSplashScreen, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun setBaseballFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}