package com.Richest.baseball.rich.player.app.screens

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.Richest.baseball.rich.player.app.R


class PlayersAdapter(private val players: ArrayList<PlayersEntity>) :
    RecyclerView.Adapter<PlayersAdapter.ViewHolder>() {

    var playersPos = 0

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val playersImg: ImageView = itemView.findViewById(R.id.playerImage)
        val playersDesc: TextView = itemView.findViewById(R.id.playerDesc)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.players_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val playersList = players[position]
        playersList.argPlayersPicture?.let { holder.playersImg.setImageResource(it) }
        holder.playersDesc.text = playersList.argPlayersDesc
        playersPos = holder.adapterPosition
    }

    override fun getItemCount(): Int = players.size
}