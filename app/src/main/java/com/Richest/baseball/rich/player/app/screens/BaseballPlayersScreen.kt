package com.Richest.baseball.rich.player.app.screens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.Richest.baseball.rich.player.app.R
import com.Richest.baseball.rich.player.app.databinding.ActivityBaseballPlayersScreenBinding

class BaseballPlayersScreen : AppCompatActivity() {

    private lateinit var playersBinding: ActivityBaseballPlayersScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        playersBinding = ActivityBaseballPlayersScreenBinding.inflate(layoutInflater)
        setContentView(playersBinding.root)
        setBaseballFullScreen()


        val playersData = ArrayList<PlayersEntity>()
        playersData.add(
            PlayersEntity(
                R.drawable.player_1,
                "CC Sabathia Net Worth - \$80 Million With an estimated net worth of \$80 million, CC Sabathia has done well for himself. Born in 1980 in California, Sabathia showed early promise as an athlete and participated in several sports, but he eventually focused on baseball."
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_2,
                "Carlos Beltran Net Worth - \$80 Million Carlos Beltran has around \$80 million dollars to his name, and to get such a high net worth, he’s earned a lot over the years. Beltan was born in Puerto Rico in 1977. He went on to play from 1998 to 2017 in the MLB."
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_3,
                "Mariano Rivera Net Worth - \$80 Million\n" +
                        "Having a net worth adding up to \$80 million, Mariano Rivera had a long and prosperous career. Rivera was born in Panama City, Panama, in 1969, and he was raised in a humble fishing village, but his talent was apparent."
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_4,
                "David Price Net Worth - \$85 Million David Price is a talented professional baseball player from Murfreesboro, Tennessee. He has accumulated a net worth of \$85 million through his impressive career."
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_5,
                "Ken Griffey, Jr. Net Worth - \$90 Million Ken Griffey Jr. is worth about \$90 million. Known as “Junior” or “the Kid,” Griffey was born in Pennsylvania in 1969. He spent most of his career with the Cincinnati Reds and the Seattle Mariners, but he also played with the Chicago White Sox. He was born to be an MLB star, as his father was also an MLB player"
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_6,
                "Gary Sheffield Net Worth - \$90 Million Gary Sheffield is worth around \$90 million. Sheffield has said, “Most people figured I’d be in jail. I’ve been proving people wrong my whole life.” Born in Tampa, Florida, Sheffield had to fight to establish a career for himself. Sheffield was known early on as having a temper, and he had to learn to calm down and focus on his talents."
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_7,
                "Felix Hernandez Net Worth - \$90 Million Felix Hernandez has earned around \$90 million for his time as an MLB player. Hernandez, known as King Felix, was born in Venezuela in 1986. He was noticed early on, at only sixteen years old. Thus, he had lots of early opportunities to hone his skills and get involved with the Mariners’ organization."
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_8,
                "Barry Bonds Net Worth - \$100 Million Worth about \$100 million, Barry Bonds has a lot of money to his name. As one of the most noteworthy all-around baseball players of all time, Barry Bonds is known for his 22 seasons in baseball. He was born in Riverside, California, in 1964, and he was the son of Bobby Bonds"
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_9,
                "Joe Mauer Net Worth- \$100 Million\n" +
                        "Joe Mauer is a catcher and first baseman who has earned himself about \$100 million. He was born in 1983 in Minnesota, so it is fitting that he spent his entire 15-season career with the Minnesota Twins."
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_10,
                "Manny Ramirez Net Worth - \$110 Million\n" +
                        "Manny Ramirez is known for his offensive skills, and he’s known as being one of the best players of his age; thus, it’s not surprising that he’s worth \$110 million. Ramirez was born in 1972 in the Dominican Republic, but he later moved to New York City when he was 13, and while he was there, he showed promise as a baseball player."
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_11,
                "Joey Votto Net Worth - \$110 Million\n" +
                        "Joey Votto was born in 1983 in Canada, and he has made his home country proud by earning a net worth of over \$110 million. He’s still playing, so this number is likely to go up."
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_12,
                "Chipper Jones Net Worth - \$110 Million With a net worth of about \$110 million, Chipper Jones, born in 1972 as Larry Wayne Jones, started as the youngest player in the majors league in 1993. His nearly two decades playing made him a lot of money and earned him a place in the Hall of Fame."
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_13,
                "Randy Johnson Net Worth - \$115 Million Randy Johnson is worth about \$115 million, which makes him one of the richest players of all time. He was born in 1963 in California. He is best known for his potent pitching. Due to hist 6’10” build, it’s no wonder that he’s known as “The Big Unit.”"
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_14,
                "Ryan Howard Net Worth - \$120 Million As of March 2023, Ryan Howard, a retired American professional baseball player, boasts a net worth of \$120 million. Over the course of his successful career, Ryan earned an estimated \$195 million in baseball salary alone."
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_15,
                "Miguel Cabrera Net Worth - \$125 Million With a net worth of \$125 million, Miguel Cabrera has done well for himself. Cabrera, known as Miggy, was born in 1983 in Venezuela, where he would start to build his skills. At sixteen, he started to get more attention and began to go through the minor league system. By 20, he began playing in the majors, and he was part of a World Series winning team."
            )
        )
        playersData.add(
            PlayersEntity(
                R.drawable.player_16,
                "Justin Verlander Net Worth - \$150 Million With such a prominent career, Justin Verlander is worth about \$150 million. Justin Verlander was born in 1983 in Virginia. He is currently playing for the New York Mets, who he recently signed an \$86.7 million contract with."
            )
        )

        playersBinding.backBtn.setOnClickListener {
            finish()
        }

        playersBinding.playersRecycler.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)

        val adapter = PlayersAdapter(playersData)
        playersBinding.playersRecycler.adapter = adapter

        playersBinding.nextBtn.setOnClickListener {
            playersBinding.playersRecycler.postDelayed(Runnable {
                playersBinding.playersRecycler.smoothScrollToPosition(adapter.playersPos + 1)
            }, 200)
            adapter.notifyDataSetChanged()
        }

    }

    private fun setBaseballFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }


}