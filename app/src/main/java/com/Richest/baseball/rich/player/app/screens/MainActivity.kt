package com.Richest.baseball.rich.player.app.screens

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.Richest.baseball.rich.player.app.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setBeautiesFullScreen()

        binding.playersBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, BaseballPlayersScreen::class.java)
            startActivity(intent)
        }

        binding.exitBtn.setOnClickListener {
            finish()
        }


    }
    private fun setBeautiesFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}